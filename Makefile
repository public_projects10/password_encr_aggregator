CC=g++
CFLAGS=-Wall -I./include/

all: pass_aggregator

pass_aggregator: obj_src obj obj/interface.o obj/vigenere_cipher.o obj/db_funcs.o
	$(CC) $(CFLAGS) obj_src/sqlite3.o obj/interface.o obj/db_funcs.o obj/vigenere_cipher.o -o pass_aggregator

obj:
	mkdir obj

obj_src:
	mkdir obj_src

obj/interface.o: obj/db_funcs.o obj/vigenere_cipher.o
	$(CC) $(CFLAGS) src/interface.cpp -c -o obj/interface.o

obj/db_funcs.o: obj_src/sqlite3.o
	$(CC) $(CFLAGS) src/db_funcs.cpp -c -o obj/db_funcs.o

obj/vigenere_cipher.o:
	$(CC) $(CFLAGS) src/vigenere_cipher.cpp -c -o obj/vigenere_cipher.o

obj_src/sqlite3.o:
	gcc $(CFLAGS) src/sqlite3.c -c -o obj_src/sqlite3.o

clean:
	rm -rf pass_aggregator obj
