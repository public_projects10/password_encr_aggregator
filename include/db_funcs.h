#ifndef DATABASE_H
#define DATABASE_H

#include <sqlite3.h>
#include <iostream>
    using namespace std;

int open_db();
void close_db();
bool check_if_table_exists();
int create_table();
int insert_domain_encr_pass_pair(string domain, string password);
string get_encr_password_by_domain(string domain);

#endif
