#include "vigenere_cipher.h"

string alphabet = "abcdefghijklmnopqrstuvwxyz1234567890.,@!";

int modulo(int a, int b){
    return a >= 0 ? a % b : (b - abs(a % b)) % b;
}

string encrypt_vigenere(string text, string key){
    int len = text.length();
    string encr_text;
    int key_index, text_index;
    for (int i = 0; i < len; i++) {
        text_index = alphabet.find(text[i]);
        key_index = alphabet.find(key[i]);
        encr_text += alphabet[(text_index + key_index) % 40];
    }
    return encr_text;
}

string decrypt_vigenere(string text, string key){
    int len = text.length();
    string decr_text;
    int key_index, text_index;
    for (int i = 0; i < len; i++) {
        text_index = alphabet.find(text[i]);
        key_index = alphabet.find(key[i]);
        decr_text += alphabet[modulo(text_index - key_index, 40)];
    }
    return decr_text;
}
