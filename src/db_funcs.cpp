#include "db_funcs.h"

sqlite3* my_db;

int open_db() {
  int errorCode = sqlite3_open("database.db", &my_db);
  if(errorCode) {
    cerr << "Error opening database: " << sqlite3_errmsg(my_db) << endl;
    sqlite3_close(my_db);
    return errorCode;
  }
  return SQLITE_OK;
}

void close_db() {
  sqlite3_close(my_db);
}


bool check_if_table_exists() {
    sqlite3_stmt *stmt;
    string query = "SELECT name FROM sqlite_master WHERE type='table' AND name='PASSWORD'";
    int result = sqlite3_prepare_v2(my_db, query.c_str(), -1, &stmt, NULL);
    if (result != SQLITE_OK) {
        cerr << sqlite3_errmsg(my_db) << endl;
        return false;
    }
    result = sqlite3_step(stmt);
    if (result == SQLITE_ROW) {
        return true;
    } else {
        return false;
    }
}


int table_exists(string table_name) {
    sqlite3_stmt *stmt;
    string query = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + table_name + "'";
    int result = sqlite3_prepare_v2(my_db, query.c_str(), -1, &stmt, NULL);
    if (result != SQLITE_OK) {
        cerr << "Error preparing statement: " << sqlite3_errmsg(my_db) << endl;
        return 0;
    }
    result = sqlite3_step(stmt);
    if (result == SQLITE_ROW) {
        return 1;
    } else {
        return 0;
    }
}


int create_table() {
  const char* query = "CREATE TABLE PASSWORD ("
    "Id INTEGER PRIMARY KEY AUTOINCREMENT,"
    "domain TEXT UNIQUE,"
    "password TEXT);";

  char* errorMessage = nullptr;
  int errorCode = sqlite3_exec(my_db, query, nullptr, nullptr, &errorMessage);
  if(errorCode) {
    cerr << "Error creating table: " << errorMessage << endl;
    sqlite3_free(errorMessage);
    return errorCode;
  }
  return SQLITE_OK;
}


int insert_domain_encr_pass_pair(string domain, string password) {
  const char* query = "INSERT INTO PASSWORD (domain, password) VALUES (?, ?);";

  sqlite3_stmt* statement;
  int errorCode = sqlite3_prepare_v2(my_db, query, -1, &statement, nullptr);
  if(errorCode != SQLITE_OK) {
    cerr << sqlite3_errmsg(my_db) << endl;
    return errorCode;
  }

  sqlite3_bind_text(statement, 1, domain.c_str(), -1, SQLITE_TRANSIENT);
  sqlite3_bind_text(statement, 2, password.c_str(), -1, SQLITE_TRANSIENT);

  errorCode = sqlite3_step(statement);
  if(errorCode != SQLITE_DONE) {
    cerr << "Insert error: " << sqlite3_errmsg(my_db) << endl;
    sqlite3_finalize(statement);
    return errorCode;
  }

  sqlite3_finalize(statement);
  return SQLITE_OK;
}


string get_encr_password_by_domain(string domain) {
  const char* query = "SELECT password FROM PASSWORD WHERE domain = ?;";

  sqlite3_stmt* statement;
  int errorCode = sqlite3_prepare_v2(my_db, query, -1, &statement, nullptr);
  if(errorCode != SQLITE_OK) {
    cerr << sqlite3_errmsg(my_db) << endl;
    return "";
  }

  sqlite3_bind_text(statement, 1, domain.c_str(), -1, SQLITE_TRANSIENT);

  errorCode = sqlite3_step(statement);
  if(errorCode != SQLITE_ROW) {
    cerr << "Cipherpass select error: " << sqlite3_errmsg(my_db) << endl;
    sqlite3_finalize(statement);
    return "";
  }

  string encr_password = reinterpret_cast<const char*>(sqlite3_column_text(statement, 0));

  sqlite3_finalize(statement);
  return encr_password;
}
