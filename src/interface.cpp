#include "interface.h"

int main(){
    open_db();
    cout << "1 - add record to db, 2 - get pass by domain, 0 - exit\n";
    bool flag = check_if_table_exists();
    if (!flag){
        create_table();
    }
    while (true){
        int c = getch();
        if (c == 48){
            close_db();
            return 0;
        }
        if ( c != 49 && c != 50 ){
            cout << "1/2" << endl;
            continue;
        }
        if ( c == 49 ){
            string domain, password;
            cout << "Enter domain - pass pair" << endl;
            cin >> domain;
            cin >> password;
            string encr_password = encrypt_vigenere(password, domain);
            insert_domain_encr_pass_pair(domain, encr_password);
            cout << "OK." << endl;
        }
        else {
            string domain;
            cout << "Enter domain" << endl;
            cin >> domain;
            string password = get_encr_password_by_domain(domain);
            password = decrypt_vigenere(password, domain);
            cout << password << endl;
        }
    }
}
